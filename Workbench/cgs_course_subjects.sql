-- Disable foreign key checks and SQL safe updates temporarily
SET SQL_SAFE_UPDATES = 0;
SET FOREIGN_KEY_CHECKS = 0;

-- Delete all data from course_subjects
DELETE FROM `course_subjects`;

-- Delete all data from course_subcourse (if exists)
DELETE FROM `course_subcourse`;


-- Delete all data from course
DELETE FROM `course`;

-- Delete all data from subject
DELETE FROM `subject`;

-- Insert Polytechnic Courses
-- Insert courses
INSERT INTO `course` (`description`, `duration`, `level`, `name`) VALUES
('Diploma in Civil Engineering', 3, 'Polytechnic', 'Polytechnic Civil'),
('Diploma in Mechanical Engineering', 3, 'Polytechnic', 'Polytechnic Mechanical'),
('Diploma in Electronics Engineering', 3, 'Polytechnic', 'Polytechnic Electronics');

-- Insert ITI Courses
INSERT INTO `course` (`description`, `duration`, `level`, `name`) VALUES
('Electrician', 2, 'ITI', 'ITI Electrician'),
('Welder', 1, 'ITI', 'ITI Welder'),
('Fitter', 2, 'ITI', 'ITI Fitter');

-- Insert Paramedical Courses
INSERT INTO `course` (`description`, `duration`, `level`, `name`) VALUES
('Diploma in Medical Laboratory Technology', 2, 'Paramedical', 'DMLT'),
('Bachelor of Science in Nursing', 4, 'Paramedical', 'B.Sc Nursing'),
('Diploma in Radiology Technology', 2, 'Paramedical', 'DRT');

-- Insert Secondary and Higher Secondary Courses
INSERT INTO `course` (`description`, `duration`, `level`, `name`) VALUES
('Secondary School Certificate', 1, 'Secondary', '10th'),
('Higher Secondary Certificate', 2, 'Higher Secondary', '12th');

-- Insert Graduate and Postgraduate Courses
-- Insert engineering courses
INSERT INTO `course` (`description`, `duration`, `level`, `name`) VALUES
('Bachelor of Technology in Computer Science Engineering', 4, 'B.Tech', 'B.Tech CSE'),
('Bachelor of Technology in Mechanical Engineering', 4, 'B.Tech', 'B.Tech Mech'),
('Bachelor of Technology in Electronics and Communication Engineering', 4, 'B.Tech', 'B.Tech ECE'),
('Bachelor of Technology in Electrical Engineering', 4, 'B.Tech', 'B.Tech EE'),
('Master of Technology in Computer Science Engineering', 2, 'M.Tech', 'M.Tech CSE'),
('Master of Technology in Mechanical Engineering', 2, 'M.Tech', 'M.Tech Mech'),
('Master of Technology in Electronics and Communication Engineering', 2, 'M.Tech', 'M.Tech ECE'),
('Master of Technology in Electrical Engineering', 2, 'M.Tech', 'M.Tech EE');

-- Insert subjects
INSERT INTO `subject` (`description`, `name`) VALUES
('Construction Management', 'Construction Management'),
('Thermodynamics', 'Thermodynamics'),
('Digital Electronics', 'Digital Electronics'),
('Electrical Circuits', 'Electrical Circuits'),
('Welding Techniques', 'Welding Techniques'),
('Machine Tools', 'Machine Tools'),
('Clinical Pathology', 'Clinical Pathology'),
('Anatomy and Physiology', 'Anatomy and Physiology'),
('Radiographic Techniques', 'Radiographic Techniques'),
('English', 'English'),
('Mathematics', 'Mathematics'),
('Science', 'Science'),
('Social Studies', 'Social Studies'),
('Computer Science', 'Computer Science');

-- Insert course-subject relationships
-- Replace subject IDs with actual subject IDs from the `subject` table
-- Assuming you have subject IDs specific to each subject
INSERT INTO `course_subjects` (`course_id`, `subjects_id`) VALUES
(1, 1),  -- Polytechnic Civil includes Construction Management
(2, 2),  -- Polytechnic Mechanical includes Thermodynamics
(3, 3),  -- Polytechnic Electronics includes Digital Electronics
(4, 4),  -- ITI Electrician includes Electrical Circuits
(5, 5),  -- ITI Welder includes Welding Techniques
(6, 6),  -- ITI Fitter includes Machine Tools
(7, 7),  -- DMLT includes Clinical Pathology
(8, 8),  -- B.Sc Nursing includes Anatomy and Physiology
(9, 9),  -- DRT includes Radiographic Techniques
(10, 10), -- 10th grade includes Mathematics
(10, 11), -- 10th grade includes Science
(10, 12), -- 10th grade includes Social Studies
(10, 13), -- 10th grade includes English
(11, 10), -- 12th grade includes Mathematics
(11, 11), -- 12th grade includes Science
(11, 12), -- 12th grade includes Social Studies
(11, 13), -- 12th grade includes English
(12, 14), -- B.Tech CSE includes English
(13, 15), -- M.Tech EE includes Computer Science
-- Additional course-subject relationships (Engineering)
(14, 16), -- B.Tech Mech includes Thermodynamics
(15, 17), -- B.Tech ECE includes Digital Electronics
(16, 18), -- B.Tech EE includes Electrical Circuits
(17, 19), -- M.Tech CSE includes Computer Science
(18, 20), -- M.Tech Mech includes Machine Tools
(19, 21) ;-- M.Tech ECE includes Radiographic Techniques

INSERT INTO `recommendation` (`id`, `level`, `percentage`, `course_id`, `rec_course_id`, `subject_id`) VALUES
(1, 'Undergraduate', 70, 1, 2, 1),   -- Recommend course 2 for subject 1 if percentage >= 70 in course 1
(2, 'Undergraduate', 75, 1, 3, 2),   -- Recommend course 3 for subject 2 if percentage >= 75 in course 1
(3, 'Undergraduate', 80, 2, 4, 3),   -- Recommend course 4 for subject 3 if percentage >= 80 in course 2
(4, 'Graduate', 70, 4, 5, 1),        -- Recommend course 5 for subject 1 if percentage >= 70 in course 4
(5, 'Graduate', 75, 4, 6, 2),        -- Recommend course 6 for subject 2 if percentage >= 75 in course 4
(6, 'Graduate', 80, 5, 7, 3);        -- Recommend course 7 for subject 3 if percentage >= 80 in course 5

SET FOREIGN_KEY_CHECKS = 1;
SET SQL_SAFE_UPDATES = 1;
