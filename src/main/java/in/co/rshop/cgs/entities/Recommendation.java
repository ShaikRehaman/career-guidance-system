package in.co.rshop.cgs.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.data.annotation.Transient;

@Entity
public class Recommendation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Double percentage;
	private String level;

	@Transient
	@ManyToOne
	private Subject subject;
	@Transient
	@ManyToOne
	private Course course;

	@Transient
	@ManyToOne
	private Course recCourse;

	public Course getRecCourse() {
		return recCourse;
	}

	public void setRecCourse(Course recCourse) {
		this.recCourse = recCourse;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

}
