package in.co.rshop.cgs.entities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String username;

	// Store the password hash instead of the plain text password
	private String passwordHash;

	private String mobile;
	private String email;
	private String address;
	private String state;
	private String country;

	@Transient
	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;

	@JsonIgnoreProperties({ "users", "subjects" })
	@Transient
	@ManyToMany(mappedBy = "users")
	private Set<Course> courses;

	@Transient
	@JsonIgnoreProperties({ "user" })
	@OneToMany(mappedBy = "user")
	private List<Marks> marks;
//
//	public String getPasswordHash() {
//		return passwordHash;
//	}
//
//	public void setPasswordHash(String passwordHash) {
//		this.passwordHash = passwordHash;
//	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		// Return the password hash instead of the plain text password
		return passwordHash;
	}

	public void setPassword(String password) {
		// Hash the password and store the hash
		this.passwordHash = hashPassword(password);
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<Marks> getMarks() {
		return marks;
	}

	public void setMarks(List<Marks> marks) {
		this.marks = marks;
	}

	// Method to hash a password using SHA-256
	private String hashPassword(String password) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] hashBytes = messageDigest.digest(password.getBytes());

			StringBuilder hashBuilder = new StringBuilder();
			for (byte hashByte : hashBytes) {
				// Convert the byte to a hexadecimal string
				String hex = Integer.toHexString(0xff & hashByte);
				if (hex.length() == 1) {
					hashBuilder.append('0');
				}
				hashBuilder.append(hex);
			}

			return hashBuilder.toString();
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Error hashing password.", e);
		}
	}

	// Method to check if the provided password matches the stored password hash
	public boolean checkPassword(String password) {
		String hashedPassword = hashPassword(password);
		return hashedPassword.equals(this.passwordHash);
	}
}
