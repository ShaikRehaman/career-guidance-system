package in.co.rshop.cgs.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.cgs.entities.Course;
import in.co.rshop.cgs.entities.Recommendation;
import in.co.rshop.cgs.entities.Subject;

public interface RecommendationsRepo extends JpaRepository<Recommendation, Long> {

	List<Recommendation> findByCourseAndSubject(Course course, Subject subject);

	List<Recommendation> findByCourseAndSubjectAndPercentageGreaterThanEqual(Course course, Subject subject,
			Double percentage);

	List<Recommendation> findByCourseIdAndSubjectIdAndPercentageGreaterThanEqual(Long courseId, Long subjectId,
			Double percentage);

//	Page<Recommendation> findByCourseAndSubjectAndPercentageGreaterThanEqualWithPageable(Course course, Subject subject,
//			Double percentage, Pageable pageable);
//
//	Page<Recommendation> findAllWithPageable(Pageable pageable);
}
