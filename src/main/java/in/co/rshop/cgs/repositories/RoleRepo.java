package in.co.rshop.cgs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.cgs.entities.Role;

public interface RoleRepo extends JpaRepository<Role, Long> {

}
