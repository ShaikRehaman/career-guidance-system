package in.co.rshop.cgs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.cgs.entities.Marks;

public interface MarksRepo extends JpaRepository<Marks, Long>{

}
