package in.co.rshop.cgs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.cgs.entities.User;

public interface UserRepo extends JpaRepository<User, Long>{

}
