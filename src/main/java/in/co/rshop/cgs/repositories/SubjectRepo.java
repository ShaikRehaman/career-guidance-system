package in.co.rshop.cgs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.cgs.entities.Subject;

public interface SubjectRepo extends JpaRepository<Subject, Long> {

}
