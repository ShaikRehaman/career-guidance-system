package in.co.rshop.cgs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.cgs.entities.Permission;

public interface PermissionRepo extends JpaRepository<Permission, Long>{

}
