package in.co.rshop.cgs.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.cgs.entities.Course;

public interface CourseRepo extends JpaRepository<Course, Long> {

}
