package in.co.rshop.cgs.service;


import in.co.rshop.cgs.entities.Course;
import in.co.rshop.cgs.entities.User;
import in.co.rshop.cgs.repositories.CourseRepo;
import in.co.rshop.cgs.repositories.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

	@Autowired
    private  CourseRepo courseRepository;


    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }

//    public Course getCourseById(Long id) {
//        Optional<Course> optionalCourse = courseRepository.findById(id);
//        return optionalCourse.orElse(null);
//    }

    public Course createCourse(Course course) {
        return courseRepository.save(course);
    }

    public Course updateCourse(Long id, Course course) {
        if (courseRepository.existsById(id)) {
            course.setId(id);
            return courseRepository.save(course);
        } else {
            return null;
        }
    }

    public void deleteCourse(Long id) {
        courseRepository.deleteById(id);
    }
    @Autowired
    public CourseService(CourseRepo courseRepo) {
        this.courseRepository = courseRepository;
    }

    public Course getUserById(Long courseId) {
        return courseRepository.findById(courseId).orElse(null);
    }

}
