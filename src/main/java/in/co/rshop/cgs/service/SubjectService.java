package in.co.rshop.cgs.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.rshop.cgs.entities.Subject;
import in.co.rshop.cgs.repositories.SubjectRepo;

@Service
public class SubjectService {

	@Autowired
	private SubjectRepo subjectRepo;
	
	public List<Subject> getAll(){
		return subjectRepo.findAll();
	}

	public Subject getSubjectById(Long id) {
		Optional<Subject> getOptional = subjectRepo.findById(id);
			return getOptional.orElseThrow(null);
	}

	public Subject createSubject(Subject subject) {
		return subjectRepo.save(subject);
	}
	
	public Subject updateSubject(Long id,Subject subject) {
		if (subjectRepo.existsById(id)) {
			subject.setId(id);
			return subjectRepo.save(subject);
		}else {
			return null;
		}
	}

	public void deleteSubject(Long id) {
		 subjectRepo.deleteById(id);
	}
}

