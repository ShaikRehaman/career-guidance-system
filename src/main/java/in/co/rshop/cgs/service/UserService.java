package in.co.rshop.cgs.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.co.rshop.cgs.entities.User;
import in.co.rshop.cgs.repositories.UserRepo;

@Service
public class UserService {

	@Autowired
    private  UserRepo userRepository;

   

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

//    public User getUserById(Long id) {
//        Optional<User> optionalUser = userRepository.findById(id);
//        return optionalUser.orElse(null);
//    }

    public User getUserByUsername(String username) {
        //return userRepository.findByUsername(username);
    	return null;
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }

    public User updateUser(Long id, User user) {
        if (userRepository.existsById(id)) {
            user.setId(id);
            return userRepository.save(user);
        } else {
            return null;
        }
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

        @Autowired
        public UserService(UserRepo userRepository) {
            this.userRepository = userRepository;
        }

        public User getUserById(Long userId) {
            return userRepository.findById(userId).orElse(null);
        }

        // Add methods to retrieve course details and marks for a user
    }


