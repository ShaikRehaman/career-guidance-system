package in.co.rshop.cgs;

import javax.persistence.EntityManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;
import javax.persistence.metamodel.Type;


@SpringBootApplication // (exclude = {SecurityAutoConfiguration.class})
@EnableSwagger2WebMvc
@Import(SpringDataRestConfiguration.class)
public class CGSApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(CGSApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(CGSApplication.class);
	}

	@Bean
    public RepositoryRestConfigurer repositoryRestConfigurer(EntityManager entityManager) {
        return RepositoryRestConfigurer.withConfig(config -> {
            config.exposeIdsFor(entityManager.getMetamodel().getEntities()
                    .stream().map(Type::getJavaType).toArray(Class[]::new));
        });
	}

	/*
	 * @Bean public void dataSource() { DriverManagerDataSource dataSource = new
	 * DriverManagerDataSource(); dataSource.setDriverClassName("org.h2.Driver");
	 * dataSource.setUrl("jdbc:h2:mydb"); dataSource.setUsername("sa");
	 * dataSource.setPassword("");
	 * 
	 * 
	 * Resource initSchema = new ClassPathResource("h2database.sql");
	 * DatabasePopulator databasePopulator = new
	 * ResourceDatabasePopulator(initSchema);
	 * DatabasePopulatorUtils.execute(databasePopulator, dataSource);
	 * 
	 * }
	 */

}
